import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { FooterFormComponent } from './footer/footer-form/footer-form.component';
import { FooterCopyrightComponent } from './footer/footer-copyright/footer-copyright.component';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    FooterFormComponent,
    FooterCopyrightComponent
  ],
  imports: [
    CommonModule,
  ],
  exports:[HeaderComponent,FooterComponent]

})
export class SharedModule { }
