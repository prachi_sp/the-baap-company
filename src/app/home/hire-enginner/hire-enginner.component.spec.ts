import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HireEnginnerComponent } from './hire-enginner.component';

describe('HireEnginnerComponent', () => {
  let component: HireEnginnerComponent;
  let fixture: ComponentFixture<HireEnginnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HireEnginnerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HireEnginnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
