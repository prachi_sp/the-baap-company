import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { HomeContentComponent } from './home-content/home-content.component';
import { HomeCounterComponent } from './home-counter/home-counter.component';
import { HomeSliderComponent } from './home-slider/home-slider.component';
import { HomeHeaderComponent } from './home-header/home-header.component';
import { SharedModule } from '../shared/shared.module';
import { AboutUsComponent } from './about-us/about-us.component';
import { HireEnginnerComponent } from './hire-enginner/hire-enginner.component';


@NgModule({
  declarations: [
    HomeComponent,
    HomeContentComponent,
    HomeCounterComponent,
    HomeSliderComponent,
    HomeHeaderComponent,
    AboutUsComponent,
    HireEnginnerComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,SharedModule
  ]
})
export class HomeModule { }
